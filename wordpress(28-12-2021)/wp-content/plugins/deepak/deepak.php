<?php
/*Custom Plugin CReate */
 
/**
 
 * @package Akismet
 */
/*
Plugin Name: Deepak
 
Plugin URI: http://localhost:8080/wordpress/

Description: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
Author: Deepak
Version: 5.8.2
Author URI: http://localhost:8080/wordpress/
 
*/


register_activation_hook(__FILE__,'form_data_activate');
register_deactivation_hook(__FILE__,'form_data_deactivate');

function form_data_activate(){

    global $wpdb;
    $table = $wpdb->prefix."deepak";
    $sql= "CREATE TABLE $table(ID INT(11) NOT NULL, NAME VARCHAR (20) NOT NULL, PRIMARY KEY(ID))";
    $wpdb->query($sql);
    
}

function form_data_deactivate(){
    global $wpdb;
    $table = $wpdb->prefix."deepak";
    $sql = "DROP TABLE $table";
    $wpdb->query($sql);
}
add_action('admin_menu', 'form_data_menu');
function form_data_menu() {
 add_menu_page('Form Data','Form Data',8,__FILE__,'form_data_list');
}
add_shortcode('form_data_list_shortcode','form_data_list');
function form_data_list() {
   include('form_data_list.php');
}