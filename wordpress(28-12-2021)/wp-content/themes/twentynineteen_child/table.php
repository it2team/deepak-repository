<?php
if( !class_exists( 'WP_List_Table') ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}
class WLT_List_table extends WP_List_Table {
	public function prepare_items() {
		$order_by = isset( $_GET['orderby']) ? $_GET['orderby'] : '';
		$order = isset( $_GET['order']) ? $_GET['order'] : '';
		$search_term = isset( $_POST['s']) ? $_POST['s'] : '';

		$this->items = $this->wlt_list_table_data( $order_by,$order,$search_term );
		$wlt_column = $this->get_columns();
		$wlt_hd_column = $this->get_hidden_columns();

		$this->_column_headers = [$wlt_column, $wlt_hd_column];
	}

	/* BULK ACTION*/
    public function get_bulk_actions() {
	$actions = [
	'bulk-delete' => 'Delete',
	'bulk-edit' => 'Edit'
	];
	return $actions;
	}
	public function handle_row_section($item, $column_name, $primary){
		if($primary !== $column_name){
			return '';
		}
		$action = [];
		$action['edit'] = '<a>'.__( 'Edit', WLT_TEXT_DOMAIN ).'</a>';
		$action['delete'] = '<a>'.__( 'Delete', WLT_TEXT_DOMAIN ).'</a>';
		$action['quick-edit'] = '<a>'.__( 'Edit', WLT_TEXT_DOMAIN ).'</a>';
		$action['view'] = '<a>'.__( 'Edit', WLT_TEXT_DOMAIN ).'</a>';
		return $this->row_actions( $action );
	}


	
	public function wlt_list_table_data($order_by = '', $order = '', $search_term = ''  ){
		?><section>
			<h2><?php _e( 'WP LIST TABLE');?></h2>
			<?php
		$data_array = [];

		$args = [
		    'post_type'      => 'post',
		    'post_status'    => 'publish',
		    'posts_per_page' => -1,
		    'fields'         => 'ids'
		];

		$my_posts = get_posts( $args );

		if( $my_posts ){
			foreach( $my_posts as $post_id ){
				$author_id = get_post_field( 'post_author', $post_id);
				$author_name = get_the_author_meta( 'display_name', $author_id);

				$data_array[] = [
					'wlt_id'           => '<a href="'.get_edit_post_link( $post_id ).'"> '.$post_id.' </a>',
					'wlt_title'        => '<a href="'.get_edit_post_link( $post_id ).'"> '.get_the_title($post_id).' </a>',
					'wlt_publish_data' => '<a href="'.get_edit_profile_url( $post_id ).'"> '.get_the_date('l F j,Y',$post_id).' </a>',
					'wlt_post_type'    => get_post_type($post_id),
					'wlt_post_author'  => '<a href="'.get_edit_profile_url( $post_id ).'"> '.$author_name.' </a>',

				];
			}
		}
		?></section><?php
		return $data_array;
	}
	public function get_hidden_columns() {
		return [];
	}
	function get_columns() {
		$columns = array (
			'cb' => '<input type="checkbox" />',
			'wlt_id' => __( 'ID', WLT_TEXT_DOMAIN ),
			'wlt_title' => __( 'Title', WLT_TEXT_DOMAIN ),
			'wlt_publish_data' => __( 'Publish Date', WLT_TEXT_DOMAIN ),
			'wlt_post_type' => __('Post Type', WLT_TEXT_DOMAIN ),
			'wlt_post_author' => __('Author Name', WLT_TEXT_DOMAIN ),
		);
		return $columns;
	}
	public function column_default( $item, $column_name )
      {
	//print_r($item);
        switch( $column_name ) {
            case 'wlt_id':
            case 'wlt_title':
            case 'wlt_publish_data':
            case 'wlt_post_type':
            case 'wlt_post_author':
             return $item[$column_name];
            default:
                return "not data found";	
        }
    }
    public function column_cb($items) {
    	$checbox = '<input type="checkbox"/>';
    	return $checbox;
    }
}
$object = new  WLT_List_table();
$object->prepare_items();
$object->display();

?>