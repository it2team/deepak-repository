<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'deepak' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xX~cuPxRavH)5_C+F`h9XVkPL*=:ZT|lRu(G!9&{iyQLsU^i_zC:>j`c`-?Ah67D' );
define( 'SECURE_AUTH_KEY',  'pmeZ2axd}FToyH^{>v|+)Xmldg|k2TA;`}Kc2ooY9dr7u>F7@CnNxSGNOhOdRRv%' );
define( 'LOGGED_IN_KEY',    'g3!R1G9KSc#8:nJV/W};oyw}ioKLV_W=EJ=fQ0zG-{XW%yo:.o.8.()wl1NdXi#^' );
define( 'NONCE_KEY',        'YVd@y{TAf~e:mX:MY$aIJ`G5n5Je)Bqjjh@(vrb`lbM!?jM.w=U}/tHAe.SLojUH' );
define( 'AUTH_SALT',        ')rp}WH;s2DTd_/zw^Bw]+uwO>X=PSbYVRiN0`3+W@@}k@_-FfoNZ2B1QfY<Bh,Jk' );
define( 'SECURE_AUTH_SALT', '0MlT[F#,eO&IsgC2#ZX]H55c]0_Hwb+QN[Unp8g|uG~kmC8kS_Ydxjq7|x1^ll4*' );
define( 'LOGGED_IN_SALT',   '6nE?.h[3#b|Y#%;:En&_c&I^)yvB0D1#[-7RP2BG 7#7+,{E?hwRmkc2D!q+6AO0' );
define( 'NONCE_SALT',       '4Y`K/s7*C.%U[9wPdaMt?bKg;_H=g~Cdpi5X+EX<O&%bI^:Yw=Z5||m}Qsufh9}y' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define( 'WP_HOME', 'http://localhost:8080/wordpress' );
define( 'WP_SITEURL', 'http://localhost:8080/wordpress' );
define('FS_METHOD','direct');


/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
