<?php 
  require_once 'config.php';
  
  if(!session_id()){
       session_start();
  }
 if (isset($_SESSION['current_user'])) {
    header("Location:dashboard.php"); 
 }

  $errors = array();
  $flag = 0;

  if(isset($_POST['login_f'])){

     if(isset($_POST['Email'])){
       $email = $_POST['Email'];
     } else {
      $email = '';
     }

     if(isset($_POST['password'])){
        $password = $_POST['password'];
     } else {
        $password = '';
     }

    //Validation Start
    if($email=='') {
        $errors[] = "enter your email !";
        $flag = 1;
     } else if(!preg_match("/^[_.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+.)+[a-zA-Z]{2,6}$/i", $email)) {
        $errors[] = "not valid email !";
        $flag = 1;
     } 

     if($password == '') {
        $errors[] = "enter password !";
        $flag = 1;
     } else if(strlen($password) < 8 ) {
        $errors[] = "Minimum 8 characters !";
        $flag = 1;
     }
     //validation end
     $sql = mysqli_query($conn, "SELECT * FROM login where email = '".$email."' AND password='".$password."'");

      if(mysqli_num_rows($sql)==1){
      $last_insert_id = mysqli_insert_id($conn);
      $_SESSION['current_user'] =   $last_insert_id; 

      header('location:dashboard.php');
      }
      else{
        $error_lg = "Wrong Credential";
      }
  }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
      <style type="text/css">
        .error {
          list-style: none;
          padding: 10px;
          background: black;
          color: red;
      }
      body{
            background: url(https://64.media.tumblr.com/tumblr_m5gl4yt2Bs1r8vu3lo1_500.gifv);
            background-size: cover;
            font-family: sans-serif;
            margin-top: 0px;
            margin-bottom:0px;
          }
      
        .main{
            width: 45%;
            margin: auto;
            margin-top: 100px;
            background: #fff;
            border: 1px solid #fff;
            border-radius: 5px;
            box-shadow: 0px 3px 17px 11px #1684e6;
        }
        h1{
          color: #000;
          font-size: 22px;
          padding: 25px 0 30px 0;
          text-align: center;
          border-bottom: 2px solid #88eef9;
        }

        .login-form .form-group {
            margin: auto;
            padding: 0 40px;
            margin-bottom: 20px;
        }

        form.login-form .row {
            margin: auto;
        }

        .login-form button.btn.btn-primary {
            margin: auto;
        }

        form.login-form {
            margin-bottom: 40px;
        }

        form.login-form .action-button{
              padding: 0 40px;
        }
        .form_text{
          width: 100%;
          overflow: hidden;
          font-size: 20px;
          padding: 8px 0;
          margin: 8px 0;
          border-color: skyblue;
        }
        .login_link{
          text-align: center;
        }
        span{
          text-align: center;
        }
      </style>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="http://localhost:8080/php/register.php">Registration</a>
                </li>
              </ul>
              <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
              </form>
            </div>
          </div>
        </nav>
    
        <div class="container main">
          <h1>Please Login Here</h1>
          <?php if(!empty($errors)){ ?>
            <ul class="error"> 
            <?php  
                foreach($errors as $error) { ?>
                <li><?php echo $error;?></li>
            <?php } ?>
            </ul>    
            <?php } ?>
          <form class="login-form" method="POST">
             <div class="row g-3"> 
              <div class="col-md-12 form-group">
                <label for="inputEmail4" class="form-label">Email</label>&nbsp;<span style="color: red;">*</span>
                <input type="text" class="form-control form_text" name="Email" id="inputEmail4"/>
              </div>
              <div class="col-md-12 form-group">
                <label for="inputPassword4" class="form-label">Password</label>&nbsp;<span style="color: red;">*</span>
                <input type="password" class="form-control form_text" name="password" id="inputPassword4">
              </div>
              <div class="col-12 action-button">
                <button type="submit" class="btn btn-primary" name="login_f">Login</button>
              </div>
            <span>Do You Have Any Account? <a href="http://localhost:8080/php/register.php">Sign Up</a></span>
            </div>
          </form>
        </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>