<?php
//left join
SELECT login.id, MOCK_DATA.fname
FROM login
OUTER left JOIN MOCK_DATA ON login.id = MOCK_DATA.id;

//right join
SELECT * FROM wp_deepak right JOIN login ON wp_deepak.ID = login.id;

//inner join
SELECT login.id, MOCK_DATA.fname
FROM login
INNER JOIN MOCK_DATA ON login.id = MOCK_DATA.id;

//
SELECT login.id, MOCK_DATA.first_name, MOCK_DATA.last_name, MOCK_DATA.gender, login.password 
FROM MOCK_DATA
INNER JOIN login ON login.id = MOCK_DATA.first_name