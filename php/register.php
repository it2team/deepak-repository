<?php
require_once "config.php";
    if(!session_id()){
       session_start();
    }
    if (isset($_SESSION['current_user'])) {
    header("Location:dashboard.php"); 
     }

    $errors = array();
    $flag = 0;

 if(isset($_POST['sign_up'])){

     if(isset($_POST['email'])){
       $email = $_POST['email'];
     } else {
      $email = '';
     }

     if(isset($_POST['password'])){
        $password = $_POST['password'];
     } else {
        $password = '';
     }

     if(isset($_POST['address'])){
        $address = $_POST['address'];
     } else {
        $address = '';
     }

     if(isset($_POST['city'])){
        $city = $_POST['city'];
     } else {
        $city = '';
     }

    //Validation Start
    if($email=='') {
        $errors[] = "enter your email !";
        $flag = 1;
     } else if(!preg_match("/^[_.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+.)+[a-zA-Z]{2,6}$/i", $email)) {
        $errors[] = "not valid email !";
        $flag = 1;
     } 

     if($password == '') {
        $errors[] = "enter password !";
        $flag = 1;
     } else if(strlen($password) < 8 ) {
        $errors[] = "Minimum 8 characters !";
        $flag = 1;
     }
     if($address == '') {
        $errors[] = "enter address !";
        $flag = 1;
     }
     if($city == '') {
        $errors[] = "enter city !";
        $flag = 1;
     }



     //validation end

    $result = mysqli_query($conn, "INSERT INTO login (email,password) VALUES ('$email','$password')");
    
    if($result){
      $last_insert_id = mysqli_insert_id($conn);
      $_SESSION['current_user'] =   $last_insert_id; 
      header('location:dashboard.php');
    }
    else
    {
      echo "Error";
    }
  }
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <style type="text/css">
      .error {
        list-style: none;
        padding: 10px;
        background: WHITE;
        color: red;
      }
      *{
        margin: 0;
        padding: 0;
        }*
        body{
          background-image: url(https://giffiles.alphacoders.com/202/202291.gif);
          background-size: cover;
          font-family: ARVO;
          margin-top: 0px;
          margin-bottom: 12%;
        }
        .reg_form{
          width: 810px;
          background-color: rgb(0,0,0,0.7);
          margin: auto;
          color: #ffff;
          padding: 10px,0px,10px,0px;
          text-align: center;
          border-radius: 15px 15px 0px 0px;
          margin-top: 10px;
        }
        .main{
          width: 810px;
          margin: auto;
          margin-top: 100px;
        }
        h1{
          color: white;
          font-weight: 100%;
          background-color: rgb(0,0,0,0.7);;
        }

        form{
          padding: 10px;
        }
        #name{
          width: 100%;
          height: 100px;
        }
        .name{
          margin-left: 25px;
          margin-top: 30px;
          width: 125px;
          color: white;
          font-size: 18px;
          font-weight: 700;
        }
      </style>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item login_link">
              <a class="nav-link active" aria-current="page" href="http://localhost:8080/php/login.php">Login</a>
            </li>
          </ul>
          <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
    <div class="container main">
      <h1>Please Registration Here</h1>   
         <?php if(!empty($errors)){ ?>
            <ul class="error"> 
            <?php
              foreach($errors as $error) { ?>
              <li><?php echo $error;?></li>
              <?php } ?>
            </ul>    
            <?php } ?>

          <form class="row g-3 reg_form" method="POST">
            <div class="col-md-6">
              <label for="inputEmail4" class="form-label">Email</label>&nbsp;<span style="color: red;">*</span>
              <input type="text" class="form-control" name="email" id="inputEmail4" placeholder="Enter a Email Address">
            </div>
            <div class="col-md-6">
              <label for="inputPassword4" class="form-label">Password</label>&nbsp;<span style="color: red;">*</span>
              <input type="password" class="form-control" name="password" id="inputPassword4" placeholder="Enter a Password">
            </div>
            <div class="col-12">
              <label for="inputAddress" class="form-label">Address</label>&nbsp;<span style="color: red;">*</span>
              <input type="text" class="form-control" id="inputAddress" name="address" placeholder="1234 Main St">
            </div>
            <div class="col-12">
              <label for="inputAddress2" class="form-label">Address 2</label>
              <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
            </div>
            <div class="col-md-6">
              <label for="inputCity" class="form-label">City</label>&nbsp;<span style="color: red;">*</span>
              <input type="text" class="form-control" name="city" id="inputCity" placeholder="Enter Your City">
            </div>
            <div class="col-12">
              <button type="submit" class="btn btn-primary sign_up" name="sign_up">Sign in</button>
            </div>
        </form>
    </div>
  </body>
</html>