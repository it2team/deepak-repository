<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {
	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 12 );


add_action('woocommerce_product_options_general_product_data', function() {

    global $post;
    echo '<div class="options_group">';

    woocommerce_wp_text_input([
        'id' => 'brand_name',
        'label' => __('Brand Name'),
        'value' => get_post_meta( $post->ID, '_brand_name', true ),
    ]);
    woocommerce_wp_text_input([
        'id' => 'size',
        'label' => __('Size'),
        'value' => get_post_meta( $post->ID, '_brand_size', true ),
    ]);
    woocommerce_wp_text_input([
        'id' => 'color',
        'label' => __('Color'),
        'value' => get_post_meta( $post->ID, '_brand_color', true ),
    ]);
    woocommerce_wp_text_input([
        'id' => 'pod',
        'label' => __('Pay On Devliver'),
        'value' => get_post_meta( $post->ID, '_pod', true ),
    ]);
    echo '</div>';
});
//Update a brand name in text field
function product_certification_price_save( $product ){
    
    if(isset($_POST['brand_name'], $_POST['size'], $_POST['color'])) {

        update_post_meta($product->get_id(), '_brand_name', sanitize_text_field( $_POST['brand_name'] ));
        update_post_meta($product->get_id(), '_brand_size', sanitize_text_field( $_POST['size'] ));
        update_post_meta($product->get_id(), '_brand_color', sanitize_text_field( $_POST['color'] ));
        update_post_meta($product->get_id(), '_pod', sanitize_text_field( $_POST['pod'] ));
    }
}
add_action( 'woocommerce_admin_process_product_object', 'product_certification_price_save', 10, 1 );

//display in frontend value name,color,size
add_action( 'woocommerce_after_add_to_cart_form', 'display_product_custom_field');
function display_product_custom_field(){
    global $product;

    $br_name = $product->get_meta('_brand_name', true ); 
    $br_size = $product->get_meta('_brand_size', true ); 
    $br_color = $product->get_meta('_brand_color', true ); 

    if( ! empty( $br_name && $br_size && $br_color) ){
        echo '<h4>'.__('Brand Name', 'woocommerce') . ': ' . $br_name . '</h4>';
        echo '<h4>'.__('Size', 'woocommerce') . ': ' . $br_size . '</h4>';
        echo '<h4>'.__('Product Color', 'woocommerce') . ': ' . $br_color . '</h4>';
    }
}

$options = get_option( 'demo-checkbox' );
if($options == 1) {
  $chk = get_option( 'demo-select' );
    if($chk == 'CFP') {
      add_action( 'woocommerce_after_add_to_cart_form', 'misha_before_add_to_cart_btn' );
        function misha_before_add_to_cart_btn(){
            global $post;
          ?>
         <form method="POST">
            $<input name="extra_price" type="text" class="add_name" id="extra_price" placeholder="Extra Price" value="<?php echo get_post_meta($post->ID, '_extra_price', true);?>">
            <input type="button" name="submit" id="MyUrlsubmit" value="ADD PRICE" class="submit">
            <div class="result"></div>
        </form>
        <?php 
            $product = wc_get_product($post->ID);
            $extra_price = get_post_meta($product->get_id(), '_extra_price', true);
            if($extra_price != ''){
                $subtotal = $product->get_price() + $extra_price;
                echo "<p><strong> Subtotal: </strong>". wc_price($subtotal)."</p>";
            }

        ?>
        <?php //ajax start form here ?>
            <script type="text/javascript">
                jQuery(document).ready(function(){
                    jQuery("#MyUrlsubmit").click(function(){
                        var extra_price = jQuery("#extra_price").val();
                        var product_id = <?php echo $post->ID;?>;
                        
                        jQuery.ajax({

                            type: 'POST',
                            url: "<?php echo admin_url('admin-ajax.php'); ?>",
                            data:{
                                'action': "savedata",
                                'extra_price': extra_price,
                                'product_id': product_id 
                            },               

                            success: function(data){
                                jQuery('.result').html('<p>'+data+'</p>');

                                if(data == 'success'){
                                    setTimeout(function(){
                                        location.reload();
                                    }, 1000);
                                }
                            }
                        });
                    });
                });
            </script>
          <?php 
        }
    }
}
//AJax Function call here
function savedata_callback(){
        
    if(isset($_POST['extra_price']) && $_POST['extra_price'] != ''){
        add_post_meta($_POST['product_id'], '_extra_price', $_POST['extra_price']);
        echo 'success';
    } else {
        echo 'Please enter value';
    }

    die();
}

add_action('wp_ajax_nopriv_savedata', 'savedata_callback');
add_action('wp_ajax_savedata', 'savedata_callback');

    add_filter('woocommerce_add_cart_item_data','wdm_add_item_data',1,10);
    function wdm_add_item_data($cart_item_data, $product_id) {

        global $woocommerce;
        $new_value = array();
        $new_value['extra_price'] = $_POST['extra_price'];

        if(empty($cart_item_data)) {
            return $new_value;
        } else {
            return array_merge($cart_item_data, $new_value);
        }
    }
