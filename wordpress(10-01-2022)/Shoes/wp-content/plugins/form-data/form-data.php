<?php
/*
Plugin name: Form Data
Author: Deepak Dorik
Author Uri: https://wordpress.com
version: 0.1
Description: Hiii this plugin for Login,logut,Registration plugin.
*/

register_activation_hook(__FILE__,'file_data_active');
register_deactivation_hook(__FILE__,'file_data_deactive');

function file_data_active(){
	global $wpdb;
	$table_prefix = $wpdb->prefix . "form_data";
	$sql = "CREATE TABLE $table_prefix(ID INT(11) NOT NULL, NAME VARCHAR (20) NOT NULL, PRIMARY KEY(ID))";
  	$wpdb->query($sql);
}

function file_data_deactive(){
	global $wpdb;
    $table = $wpdb->prefix . "form_data";
    $sql = "DROP TABLE $table";
    $wpdb->query($sql);
}

add_action('admin_menu', 'form_data_menu');
function form_data_menu() {
 add_menu_page('Form Data','Form Data',8,__FILE__,'form_data_list');
}

add_shortcode('form_data_list_shortcode','form_data_list');
function form_data_list() {
   include('form_data_list.php');
}

// add_filter( 'theme_page_templates', 'sf_add_page_template_to_dropdown1' );
// function sf_add_page_template_to_dropdown1( $templates )
// {
//    $templates[plugin_dir_path( __FILE__ ) . '/form_data_list.php'] = __( 'Login', 'text-domain' );

//    return $templates;
// }
?>