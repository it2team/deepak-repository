<?php
/*
Plugin name: Cart Menu
Author: Deepak Dorik
Author Uri: https://wordpress.com
version: 0.1
Description: Hiii this plugin for Cart Customization.
*/

register_activation_hook(__FILE__,'file_data_active');
register_deactivation_hook(__FILE__,'file_data_deactive');

// add_action('admin_menu', 'file_data_active');
// function file_data_active() {
//  add_menu_page('Donation Cart', 'Donation Cart', 'manage_options', 'my-menu', 'form_data_list' );
//  add_submenu_page('my-menu', 'Setting', 'Setting', 'manage_options', 'my-menu' ,'form_data_list');
//  add_submenu_page('my-menu', 'Donation', 'Donation', 'manage_options', 'my-menu2','form_data_list' );
//  /************add function include a file with form_data_list*********/
//  add_shortcode('form_data_list_shortcode','form_data_list');
//  function form_data_list() {
//    include('cart_data.php');
//  }
// }

add_action('admin_menu', 'file_data_active');
function file_data_active() {

add_submenu_page("options-general.php", "Demo", "Demo", "manage_options", "demo", "demo_page");


function demo_settings_page()
{
    add_settings_section("section", "Section", null, "demo");
    add_settings_field("demo-checkbox", "Enable/Disable", "demo_checkbox_display", "demo", "section");  
    register_setting("section", "demo-checkbox");
    //Section for select dropdown box
    add_settings_section("section", "Section", null, "demo");
    add_settings_field("demo-select", "Product Add", "demo_select_display", "demo", "section");  
    register_setting("section", "demo-select");
}

function demo_checkbox_display()
{
   ?>
        <input type="checkbox" name="demo-checkbox" value="1" <?php checked(1, get_option('demo-checkbox'), true); ?> />
   <?php
}

add_action("admin_init", "demo_settings_page");
function demo_page()
{
  ?>
      <div class="wrap">
         <h1>Cart</h1>
         <form method="post" action="options.php">
            <?php
               settings_fields("section");
               do_settings_sections("demo");                
               submit_button();
            ?>
         </form>
      </div>
   <?php
}

function demo_settings()
{
    add_settings_section("section", "Section", null, "demo");
    add_settings_field("demo-select", "Demo Select Box", "demo_select_display", "demo", "section");  
    register_setting("section", "demo-select");
}

function demo_select_display()
{
   ?>
        <select name="demo-select">
          <option value="CFP" <?php selected(get_option('demo-select'), "CFP"); ?>>Contribution For Product</option>
          <option value="CAC" <?php selected(get_option('demo-select'), "CAC"); ?>>Contribution For Cart</option>
        </select>
   <?php
}
}