<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'shoes' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ')JTOi]I;KMB(:5v5nuE@i.&NQ6~xvwm}VEfigKG9nu.q^{UUzcT0~SP.~ >@owFJ' );
define( 'SECURE_AUTH_KEY',  '@q^bAP|/+W&7z[SgP)Wd~!FV2wve1HgLe&^*e2MHMF(x>N)HeIs[CfJLD>he)=n$' );
define( 'LOGGED_IN_KEY',    '_p[/|<BstPE%oss|}7VCX;)yq/Q-<E#U+|9#V59XlR=?T6PG@kt{rV(@nrC(w.@m' );
define( 'NONCE_KEY',        'p%+7lB Q%Mt%D-U3rGvsDvs^!vw6L(cLoz@Rgtqi{mj:bq5b_+ps97pT<9p%4py/' );
define( 'AUTH_SALT',        'GD&?SfFT7f3TgA -5}z,hw8]x_<z-]JmzBNT)j+`ORTvLE&7E&_9|@-N?{$eLGis' );
define( 'SECURE_AUTH_SALT', 'ePlcqSoXVWThSb4),pi5/#n><C^;B_*m~I(!0kfv.S*dlEe5:F6jqW,w_-@NvK8;' );
define( 'LOGGED_IN_SALT',   'K Q<otA&5J:).}?aA8E7E1mR3X}hU9i+)^$OKl;3pSADJ~/a$:)a/~.[+/nBt18(' );
define( 'NONCE_SALT',       'v,^cCaFCy`?6eITgW4Oz*kMWR9g~SY_l6&sp`FqcF*0FZz)6!-~1,rG=E,qM9c_A' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
    
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
define('FS_METHOD', 'direct');