<?php 
/*
Plugin Name: WP Table 
Description: Create a table in backend to title,description,bul and all things
Plugin URI: http://localhost/wordpress
Author URI: http://localhost/wordpress
Author: Deepak
*/
if( !defined( 'ABSPATH' ) ); exit;

define( 'WLT_DIR', plugin_dir_path ( __FILE__) );
define( 'WLT_INCLUDES_DIR',trailingslashit( WLT_DIR . 'includes' ) );
define( 'WLT_TEXT_DOMAIN', 'wp-list-table' );
